A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
cacti
--
chromium (dilinger)
--
cryptojs
--
dnsdist (jmm)
--
frr
--
gpac/oldstable
--
gtkwave
--
h2o (jmm)
--
libreswan (jmm)
  Maintainer prepared bookworm-security update, but needs work on bullseye-security backports
--
linux (carnil)
  Wait until more issues have piled up, though try to regulary rebase for point
  releases to more recent v5.10.y and 6.1.y versions
--
nbconvert/oldstable
  Guilhem Moulin proposed an update ready for review
--
openjdk-17 (jmm)
  latest release needs backport of jtreg7 for bookworm
--
php-cas/oldstable
--
php-dompdf-svg-lib/stable
--
php-horde-mime-viewer/oldstable
--
php-horde-turba/oldstable
--
phppgadmin
--
pillow (jmm)
--
py7zr/oldstable
--
python-asyncssh
--
redmine/stable
--
ring
  might make sense to rebase to current version
--
ruby2.7/oldstable
  Utkarsh Gupta offered help in preparing updates
--
ruby3.1/stable
--
ruby-nokogiri/oldstable
--
ruby-rails-html-sanitizer
--
ruby-sanitize
  Abhijith PA proposed an update for review for bookworm-security, asked back for bullseye-security
--
ruby-sinatra/oldstable
  Maintainer posted packaging repository link with proposed changes for review
--
ruby-tzinfo/oldstable
--
salt/oldstable
--
samba/oldstable
  santiago started to backport patches to bullseye
--
squid (apo)
--
varnish
--
zabbix
--
zbar (carnil)
  Prepared update but needs some additional testing before the release
--
